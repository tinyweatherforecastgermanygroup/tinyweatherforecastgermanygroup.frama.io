---
title: Tiny Weather Forecast Germany
description: Eine deutsche Open Source Android Wetter App 
---

# [TinyWeatherForecastGermany (TWFG)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html)

!!! info "Über"
    Tiny Weather Forecast Germany, zu deutsch: *Kleine Wettervorschau Deutschland*, ist eine in :fontawesome-brands-java: `Java` geschriebene quelloffene Wettervorhersage-App für Android (ab 4.4), die sich auf Deutschland konzentriert[^1] und offene Daten des Deutschen Wetterdienstes (DWD) verwendet.

    :material-lightbulb-on: Anders als vergleichbare Apps aus dem :simple-fdroid: F-Droid Store kann TinyWeatherForecastGermany  :material-watch: smarte Gadgets (Smart Watches oder Armbänder) via [**Gadgetbridge**](https://gadgetbridge.org/) mit Wetterdaten versorgen. Dabei ist es **nicht** auf die [**OpenWeatherMap**](https://openweathermap.org/price) API-Schnittstelle angewiesen. Sie benötigen also **keinen** API-Schlüssel und müssen kein Tages-Limit beim Daten-Abruf beachten. :fontawesome-regular-face-grin:

**Maintainer**: Pawel Dube ([@Starfish](https://codeberg.org/Starfish)) :octicons-star-16: :material-fish:

[:simple-codeberg: code repository](https://codeberg.org/Starfish/TinyWeatherForecastGermany/){ .md-button }

[:simple-fdroid: F-Droid-Store Seite](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany){ .md-button }

## :octicons-location-24: Locations

[:material-home-search: durchsuchbare Tabelle aller Gemeinden, Städte, Seen, etc. die in der Datenbank des DWD gelistet sind -> *"areas"*](https://tinyweatherforecastgermanygroup.gitlab.io/index/areas.html)

[:material-home-search: durchsuchbare Tabelle aller Messstationen -> *"stations"*](https://tinyweatherforecastgermanygroup.gitlab.io/index/stations.html)

[:material-map-plus: OpenStreetMap-basierte **Karte**, die alle Datenquellen visualisiert, die von TWFG genutzt werden](https://tinyweatherforecastgermanygroup.gitlab.io/index/map.html)

## :material-shield-lock: Privatsphäre

[offizieller **ExodusPrivacy** Scan-Bericht des neusten :simple-fdroid: **F-Droid** releases](https://reports.exodus-privacy.eu.org/de/reports/de.kaffeemitkoffein.tinyweatherforecastgermany/latest/)

[inoffizieller **ExodusPrivacy** Scan-Bericht der neusten :material-android: apk](https://twfgcicdbot.github.io/TinyWeatherForecastGermanyScan/index.html)

## :material-frequently-asked-questions: FAQ

[:material-frequently-asked-questions: häufig gestellte Fragen (**FAQ**)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html#faq)

[:octicons-code-24: :material-help: javadoc **code Dokumentation**](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/index.html)

??? info "note"
    Die **javadoc** Dokumentation wird automatisch **einmal pro Woche** aktualisiert.

    Die Docs enthalten auch :fontawesome-solid-diagram-project: **UML** Diagramme. Diese werden mit [graphviz](https://gitlab.com/graphviz/graphviz) via [PlantUML](https://plantuml.com/) über das [umldoclet](https://github.com/talsma-ict/umldoclet) generiert.

## :octicons-mirror-24: Mirror-Repositories

!!! info "Hinweis"
    Die Folgenden Git-Repositories werden **einmal täglich** aktualisiert.

    Ziel: Erhöhung der SEO-Scores und neue Nutzer*innen auf das "Haupt-code-Repository" unter [:simple-codeberg: codeberg.org](https://codeberg.org/Starfish/TinyWeatherForecastGermany/) führen. Dort wird die App weiterentwickelt, es werden gerne Fragen beantwortet und Anregungen entgegen genommen.

    In Spitzenzeiten kann ein zusätzlicher Pipeline-Plan manuell aktiviert werden, der alle 8 Stunden eine Aktualisierung durchführt.

[:material-gitlab: **GitLab** Mirror](https://gitlab.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^2]

[:material-github: **GitHub** Mirror](https://github.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^2]

[:material-git: **framagit** Mirror](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanymirror)[^2]

[:simple-gitea: **Gitea** Mirror](https://gitea.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermanyMirror)[^1]

Wenn Sie ein **neues** Mirror-Repository hinzufügen möchten, auf einem gehosteten Forgejo, GitLab, GitHub Enterprise, Gitea, Gogs oder anderen :fontawesome-brands-git: Git-Server, wenden Sie sich bitte an Jean-Luc Tibaux ([@eugenoptic44](https://codeberg.org/eUgEntOptIc44)) :material-email:.

## :material-list-status: Status

Wir betreiben eine [**Status** Seite](https://tinyweatherforecastgermanygroup.github.io/statuspage/) mit [GitHub](https://github.com/tinyweatherforecastgermanygroup/statuspage) Pages, [upptime](https://upptime.js.org/) und shared runners von GitHub.

## :material-translate: Übersetzungen

Die **Übersetzungen** von TinyWeatherForecastGermany werden auf der von Codeberg e.V. zur Verfügung gestellten :simple-weblate: Weblate[^3]-Instanz verwaltet. Jede*r ist eingeladen uns beim Übersetzen zu unterstützen! Vorschläge können auch **ohne** einen Account eingereicht werden. Für das Übernehmen von Übersetzungen in den Quellcode wird ein **kostenloser** Account benötigt.

[![Weblate translation status](https://translate.codeberg.org/widgets/tiny-weather-forecast-germany/de/multi-blue.svg "Weblate translation status of TinyWeatherForecastGermany"){ loading="lazy" decoding="async" }](https://translate.codeberg.org/engage/tiny-weather-forecast-germany/de/){ title="Weblate translation status" }

[^1]: Der DWD stellt aber auf der gleichen Platform auch die Daten der Weltorganisation für Meteorologie (WMO) zur Verfügung. Daher lassen sich für alle Stationen der WMO-Mitglieder über die App Daten abrufen. Die Regenvorhersage ist jedoch nur von den deutschen Stationen verfügbar.
[^2]: von Jean-Luc Tibaux mit Hilfe eines geplanten GitLab CI/CD-Jobs :octicons-rocket-24: gesteuert
[^3]: ein :material-open-source-initiative: Open Source (quelloffener) Software-as-a-Service-Dienst (SaaS) für gemeinschaftliche Übersetzungen von Anwendungen und Webseiten.

*[DWD]: Deutscher Wetterdienst
*[TWFG]: inoffizielle Abkürzung für Tiny Weather Forecast Germany
*[WMO]: Weltorganisation für Meteorologie/World Meteorological Organization
*[SaaS]: Software-as-a-Service
