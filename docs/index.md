---
title: Tiny Weather Forecast Germany
description: An open source android weather app focused on Germany
---

# [TinyWeatherForecastGermany (TWFG)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html)

!!! info "about"
    An android open source weather forecast app written in :fontawesome-brands-java: `java` focused on Germany using open data provided by Deutscher Wetterdienst (DWD). Being a member of the World Meterological Organization (WMO), DWD also provides weather data shared by other WMO members. Please see this [map](https://tinyweatherforecastgermanygroup.gitlab.io/index/map.html) for an overview of all covered weather stations.

    :material-lightbulb-on: Compared to other open source weather apps on the :simple-fdroid: F-Droid store TinyWeatherForecastGermany does support the data exchange with :material-watch: smart gadgets via [**Gadgetbridge**](https://gadgetbridge.org/) and does **not** rely on the rate-limited [**OpenWeatherMap**](https://openweathermap.org/price) api. :fontawesome-regular-face-grin:

**Maintainer**: Pawel Dube ([@Starfish](https://codeberg.org/Starfish)) :octicons-star-16: :material-fish:

[:simple-codeberg: code repository](https://codeberg.org/Starfish/TinyWeatherForecastGermany/){ .md-button }

[:simple-fdroid: F-Droid page](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany){ .md-button }

## :octicons-location-24: Locations

[:material-home-search: searchable table of all warning *areas* in Germany](https://tinyweatherforecastgermanygroup.gitlab.io/index/areas.html)

[:material-home-search: searchable table of all weather **stations**](https://tinyweatherforecastgermanygroup.gitlab.io/index/stations.html)

[:material-map-plus: OpenStreetMap based **map** visualizing all data sources used by TWFG](https://tinyweatherforecastgermanygroup.gitlab.io/index/map.html)

## :material-shield-lock: Privacy

[official **ExodusPrivacy** scan of latest :simple-fdroid: **F-Droid** release](https://reports.exodus-privacy.eu.org/de/reports/de.kaffeemitkoffein.tinyweatherforecastgermany/latest/)

[inofficial **ExodusPrivacy** scan of latest release :material-android: apk](https://twfgcicdbot.github.io/TinyWeatherForecastGermanyScan/index.html)

## :material-frequently-asked-questions: FAQ

[:material-frequently-asked-questions: list of frequently asked questions (**FAQ**)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html#faq)

[:octicons-code-24: :material-help: javadoc **code documentation**](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/index.html)

??? info "note"
    The **javadoc** docs are automatically updated **once daily**.

    The docs also contain :fontawesome-solid-diagram-project: **UML** diagrams generated using [graphviz](https://gitlab.com/graphviz/graphviz) via [PlantUML](https://plantuml.com/) integrated in the [umldoclet](https://github.com/talsma-ict/umldoclet).

## :octicons-mirror-24: Mirrors

???+ info "note"
    The following git repositories are updated **once daily** at 5am UTC.

    Target: increased SEO-Scores, leading interested members of the public to the 'main' project at [codeberg.org](https://codeberg.org/Starfish/TinyWeatherForecastGermany/)

    During peak times an additional pipeline schedule covering every 8h may be manually activated.

[:material-gitlab: **GitLab** Mirror](https://gitlab.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^1]

[:material-github: **GitHub** Mirror](https://github.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^1]

[:material-git: **framagit** Mirror](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanymirror)[^1]

[:simple-gitea: **Gitea** Mirror](https://gitea.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermanyMirror)[^1]

If you'd like to add a **new** mirror repository on a hosted Forgejo, GitLab, GitHub Enterprise, Gitea, Gogs instance or any other :fontawesome-brands-git: server please get in touch with Jean-Luc Tibaux ([@eugenoptic44](https://codeberg.org/eUgEntOptIc44)) :material-email:.

## :material-list-status: Status

We provide a [**status** page](https://tinyweatherforecastgermanygroup.github.io/statuspage/) at [GitHub](https://github.com/tinyweatherforecastgermanygroup/statuspage) Pages powered by [upptime](https://upptime.js.org/) and GitHub shared runners.

## :material-translate: Translations

Translations of **TinyWeatherForecastGermany** are managed on the :simple-weblate: **Weblate**[^2] server generously provided by Codeberg e.V. Everyone is invited to help us translate! Suggestions for translations can also be submitted **without** an account. A **free** account is required to add translations to the source code.

[![Weblate translation status](https://translate.codeberg.org/widgets/tiny-weather-forecast-germany/-/multi-blue.svg "Weblate translation status of TinyWeatherForecastGermany"){ loading="lazy" decoding="async" }](https://translate.codeberg.org/engage/tiny-weather-forecast-germany/){ title="Weblate translation status" }

[^1]: maintained by Jean-Luc Tibaux using a scheduled GitLab CI/CD job :octicons-rocket-24:
[^2]: an open source SaaS for community translations of apps and websites.

*[DWD]: Deutscher Wetterdienst (the German national weather agency similar to NOAA in the US)
*[TWFG]: inofficial abbreviation for TinyWeatherForecastGermany
*[SaaS]: Software as a Service
*[FAQ]: frequently asked questions
*[WMO]: World Meterological Organization
