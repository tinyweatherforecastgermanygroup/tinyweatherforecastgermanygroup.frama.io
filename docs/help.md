---
title: Tiny Weather Forecast Germany
description: An open source android weather app focused on Germany
---

# Help

## :material-keyboard: Shortcuts

!!! help "shortcuts"
    Type ++s++ or ++f++ to **search** this site.
    Type ++p++ or ++comma++ to go the **previous** page.
    Type ++n++ or ++period++ to go the **next** page.

## :material-translate: Translations

Please visit the git repository at [framagit.org](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanygroup.frama.io) to suggest further translations or modifications of this page.
