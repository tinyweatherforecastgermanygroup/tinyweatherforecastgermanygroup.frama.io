---
title: Tiny Weather Forecast Germany
description: Une petite applis android de prévisions météorologiques pour l'Allemagne
---

# [TinyWeatherForecastGermany (TWFG)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html)

!!! info "à propos"
    Tiny Weather Forecast Germany, Tiny Météo Allemagne en français, est une application :material-open-source-initiative: open source de prévisions météorologiques pour Android (4.4 ou plus) écrite en `Java` qui se concentre sur l'Allemagne[^1] et utilise les données ouvertes du Deutscher Wetterdienst (DWD) le service météorologique allemand.

    :material-lightbulb-on: Par rapport aux autres applications météo open source disponibles sur :simple-fdroid: F-Droid store, TinyWeatherForecastGermany prend en charge l'échange de données avec les gadgets smart :material-watch: via [**Gadgetbridge**](https://gadgetbridge.org/) et ne s'appuie **pas** sur l'api à tarif limité [**OpenWeatherMap**](https://openweathermap.org/price). :fontawesome-regular-face-grin:

**Maintainer**: Pawel Dube ([@Starfish](https://codeberg.org/Starfish)) :octicons-star-16: :material-fish:

[:simple-codeberg: dépôt de code](https://codeberg.org/Starfish/TinyWeatherForecastGermany/){ .md-button }

[:simple-fdroid: F-Droid page](https://f-droid.org/packages/de.kaffeemitkoffein.tinyweatherforecastgermany){ .md-button }

## :octicons-location-24: Sites

[:material-home-search: tableau consultable de tous les domaines météo -> *"areas"*](https://tinyweatherforecastgermanygroup.gitlab.io/index/areas.html)

[:material-home-search: tableau consultable de toutes les **stations**](https://tinyweatherforecastgermanygroup.gitlab.io/index/stations.html)

[:material-map-plus: Une **carte** basée sur OpenStreetMap visualisant toutes les sources de données utilisées par TWFG](https://tinyweatherforecastgermanygroup.gitlab.io/index/map.html)

## :material-shield-lock: Protection de la vie privée

[analyse officielle **ExodusPrivacy** de la dernière :simple-fdroid: **F-Droid** version](https://reports.exodus-privacy.eu.org/de/reports/de.kaffeemitkoffein.tinyweatherforecastgermany/latest/)

[analyse inofficielle **ExodusPrivacy** de la dernière :material-android: apk](https://twfgcicdbot.github.io/TinyWeatherForecastGermanyScan/index.html)

## :material-frequently-asked-questions: FAQ

[:material-frequently-asked-questions: liste des questions fréquemment posées (**FAQ**)](https://tinyweatherforecastgermanygroup.gitlab.io/index/index.html#faq)

[:octicons-code-24: :material-help: javadoc **documentation du code**](https://tinyweatherforecastgermanygroup.gitlab.io/twfg-javadoc/index.html)

??? info "note"
    Les documents **javadoc** sont automatiquement mis à jour **plusieurs fois par semaine**.

    La documentation contient également :fontawesome-solid-diagram-project: Diagrammes **UML** générés à l'aide de [graphviz](https://gitlab.com/graphviz/graphviz) via [PlantUML](https://plantuml.com/) intégré dans le [umldoclet](https://github.com/talsma-ict/umldoclet).

## :material-list-status: État des ressources

Nous fournissons une [page d'état](https://tinyweatherforecastgermanygroup.github.io/statuspage/) sur une GitHub Pages site, fonctionnant grâce à [upptime](https://upptime.js.org/) et aux runners partagés de [GitHub](https://github.com/tinyweatherforecastgermanygroup/statuspage).

## :octicons-mirror-24: Mirroirs

!!! info "note"
    Les dépôts git suivants sont mis à jour **plusieurs fois par semaine**.

    Objectif: augmenter les scores de référencement, en dirigeant les personnes intéressées vers le projet "principal" à l'adresse [:simple-codeberg: codeberg.org](https://codeberg.org/Starfish/TinyWeatherForecastGermany/).

    Pendant les périodes de pointe, un programme supplémentaire de pipelines couvrant toutes les 8 heures peut être activé manuellement.

[:material-gitlab: **GitLab** Mirroir](https://gitlab.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^2]

[:material-github: **GitHub** Mirroir](https://github.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermany)[^2]

[:material-git: **framagit** Mirroir](https://framagit.org/tinyweatherforecastgermanygroup/tinyweatherforecastgermanymirror)[^2]

[:simple-gitea: **Gitea** Mirror](https://gitea.com/tinyweatherforecastgermanygroup/TinyWeatherForecastGermanyMirror)[^1]

Si vous souhaitez ajouter un **nouveau** dépôt miroir sur un hébergement Forgejo, GitLab, GitHub Enterprise, Gitea, Gogs instance ou tout autre serveur :fontawesome-brands-git:, veuillez s.v.p. contacter Jean-Luc Tibaux ([@eugenoptic44](https://codeberg.org/eUgEntOptIc44)) :material-email:.

## :material-translate: Traductions

Les traductions de TinyWeatherForecastGermany sont gérées sur l'instance :simple-weblate: weblate[^3] maintenue par Codeberg e.V. Tout le monde est invité à nous aider à traduire ! Les propositions peuvent également être soumises **sans** compte. Un compte **gratuit** est nécessaire pour transférer les traductions dans le code source.

[![weblate translation status](https://translate.codeberg.org/widgets/tiny-weather-forecast-germany/fr/multi-blue.svg "états des traductions weblate de TinyWeatherForecastGermany"){ loading="lazy" decoding="async" }](https://translate.codeberg.org/engage/tiny-weather-forecast-germany/fr/){ title="états des traductions sur weblate" }

[^1]: Cependant, le DWD fournit également les données de l'Organisation météorologique mondiale (OMM) sur la même plateforme. Les données de toutes les stations des membres de l'OMM peuvent donc être récupérées via l'application. Cependant, les prévisions de pluie ne sont disponibles que sur les stations allemandes.
[^2]: maintenu par Jean-Luc Tibaux à l'aide d'une tâche GitLab CI/CD programmée :octicons-rocket-24:
[^3]: un logiciel :material-open-source-initiative: open source (libre) en tant que service (SaaS) pour les traductions communautaires d'applications et de sites web.

*[DWD]: Deutscher Wetterdienst - service météorologique national d'allemagne
*[TWFG]: TinyWeatherForecastGermany - Tiny Météo Allemagne
*[WMO]: Organisation météorologique mondiale/World Meteorological Organization
*[SaaS]: Software as a Service
