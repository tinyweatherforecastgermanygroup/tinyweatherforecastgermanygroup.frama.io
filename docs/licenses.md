---
title: TinyWeatherForecastGermany - frama Pages - Licenses
description: The licenses of all python modules used to build this site.
---

# :material-copyright: Licenses

The licenses of all python modules used to build this site. A big thank you ❤ to all maintainers and contributors of these projects. We wouldn't be able to build sites like this one without them.

???+ info "notice"
    This table is generated **autonomously** through [`pip-licenses`](https://github.com/raimon49/pip-licenses) by parsing the metadata of the modules in the virtual environment. Errors might occur.


