---
title: Tiny Weather Forecast Germany
description: Eine deutsche Open Source Android Wetter App 
---

# Hilfe

!!! help "Tastatur-Shortcuts"
    ++s++ oder ++f++ aktiviert die **Suche**
    ++p++ oder ++comma++ um zur **vorherigen** Seite zu wechseln
    ++n++ oder ++period++ um zur **nächsten** Seite zu wechseln
